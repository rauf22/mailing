from django.db import models

from mailing_lists.models import Mailing_lists


class Clients(models.Model):
    phone = models.IntegerField(verbose_name='Телефон')
    oper_code = models.IntegerField(verbose_name='Код опереатора') # blank=True
    teg = models.CharField(max_length=10, verbose_name='Тег(Произвольная метка)')
    utc = models.IntegerField(verbose_name='Часовой пояс')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    mailing_list = models.ForeignKey(Mailing_lists, on_delete=models.PROTECT, null=True)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        ordering = ['-created_at']

    def __int__(self):
        return self.phone