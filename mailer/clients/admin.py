from django.contrib import admin

from.models import Clients

class ClientsAdmin(admin.ModelAdmin):
    list_display = ('id', 'phone', 'oper_code', 'teg', 'utc', 'created_at')
    list_display_links = ('id', 'phone')
    search_fields = ('phone', 'created_at')

admin.site.register(Clients, ClientsAdmin)
