from django.shortcuts import render
from django.http import HttpResponse
from .models import Clients


def index(request):
  # print(dir(request))
  clients = Clients.objects.all()
  # res = '<h1>Список пользователей</h1>'
  # for item in clients:
  #   res += f'<div>\n<p>{item.phone}</p>\n</div>\n<hr>\n'
  # return HttpResponse(res)
  return render(request, 'clients/index.html', {'clients': clients, 'title': 'Список клиентов'})


def test(request):
  return HttpResponse('<h1>Тестовая страницв </h1>')