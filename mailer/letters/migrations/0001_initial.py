# Generated by Django 3.2.15 on 2022-08-26 12:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('mailing_lists', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Letters',
            fields=[
                ('message', models.TextField(verbose_name='сообщение')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='дата и время создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('mailing_list', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='mailing_lists.mailing_lists')),
            ],
            options={
                'verbose_name': 'Cooбщение',
                'verbose_name_plural': 'Cooбщения',
            },
        ),
    ]
