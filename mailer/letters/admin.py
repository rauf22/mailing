from django.contrib import admin
from .models import Letters
# from mailing_lists.models import Mailing_lists


class LettersAdmin(admin.ModelAdmin):
    list_display = ('message', 'mailing_list', 'created_at')
    list_display_links = ('message',)
    search_fields = ('created_at',)


admin.site.register(Letters, LettersAdmin)