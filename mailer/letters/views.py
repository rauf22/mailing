from django.shortcuts import render
from .models import Letters
from django.http import HttpResponse

def index(request):
  letters = Letters.objects.all()
  return render(request, 'letters/index.html', {'letters': letters, 'title': 'Сообщения'})
