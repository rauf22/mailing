from django.db import models
from mailing_lists.models import Mailing_lists

class Letters(models.Model):
    # date_time_send = models.DateTimeField(verbose_name='дата и время (отправки)')
    # status_send = models.BooleanField(verbose_name='статус отправки')
    message = models.TextField(verbose_name='сообщение')
    # mailing_list_id = models.IntegerField(verbose_name='id рассылки')
    # client_id = models.CharField(max_length = 10, verbose_name='id клиента')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='дата и время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    mailing_list = models.OneToOneField(Mailing_lists, on_delete=models.CASCADE,
        primary_key=True, verbose_name= 'Рассылка')

    class Meta:
        verbose_name = 'Cooбщение'
        verbose_name_plural = 'Cooбщения'

    def __str__(self):
      return self.message
