# Generated by Django 3.2.15 on 2022-08-30 17:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mailing_lists', '0005_mailing_lists_message'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailing_lists',
            name='client_oper_code',
            field=models.IntegerField(default=0, verbose_name='Код мобильного оператора клиента'),
        ),
    ]
