from django.contrib import admin
from .models import Mailing_lists

class Mailing_listsAdmin(admin.ModelAdmin):
    list_display = ('date_time_start', 'name', 'message', 'r_teg', 'client_teg', 'r_oper_code', 'client_oper_code', 'date_time_finish', 'created_at')
    list_display_links = ('date_time_start', 'name')
    search_fields = ('date_time_start', 'created_at')
    list_editable = ('date_time_finish',)
    list_filter = ('date_time_start',)

admin.site.register(Mailing_lists, Mailing_listsAdmin)
