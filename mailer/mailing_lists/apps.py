from django.apps import AppConfig


class MailingListsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mailing_lists'
    verbose_name = 'Рассылки'
