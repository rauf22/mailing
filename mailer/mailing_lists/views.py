from django.shortcuts import render, redirect
from django.http import HttpResponse

from .models import Mailing_lists
from .forms import AddMailingListForm


def index(request):
  mailing_lists = Mailing_lists.objects.all()
  return render(request, 'mailing_lists/index.html', {'mailing_lists': mailing_lists, 'title': 'Рассылка'})

def create(request):
  if request.method == 'POST':
    form = AddMailingListForm(request.POST)
    if form.is_valid():
      form.save()
      return redirect('/')
  else:
    form = AddMailingListForm()
    return render(request, 'mailing_lists/create.html', {'form': form, 'title': 'Добавить Рассылку' })
