from django import forms
from .models import *
from clients.models import *


class AddMailingListForm(forms.ModelForm):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
  
  class Meta:
    model = Mailing_lists
    fields = ['date_time_start', 'name', 'message', 'r_teg', 'client_teg', 'r_oper_code', 'client_oper_code', 'date_time_finish']
    # widgets = {
    #   'message': forms.Textarea(attrs={'cols': 60, 'rows': 10}),
    #   }