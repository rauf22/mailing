from django.db import models
# from clients.models import Clients


class Mailing_lists(models.Model):
    date_time_start = models.DateTimeField(verbose_name='дата и время запуска рассылки')
    message = models.TextField(verbose_name='текст сообщения для доставки клиенту', default=0)
    name = models.CharField(max_length=50, verbose_name='Наименование рассылки')
    r_teg = models.BooleanField(verbose_name='Рассылка по тегу', default=0)
    r_oper_code = models.BooleanField(verbose_name='Рассылка по коду мобильного оператора', default=0)
    client_teg = models.CharField(max_length=10, verbose_name='Teg клиента', default=0)
    client_oper_code = models.IntegerField(verbose_name='Код мобильного оператора клиента', default=0)
    date_time_finish = models.DateTimeField(max_length=10, verbose_name='дата и время окончания рассылки')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='дата и время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
        ordering = ['-created_at']

    def __str__(self):
      return self.name

    # def client_code():
    #   client_oper_code = Mailing_lists.clients.oper_code
    #   Mailing_lists.client_teg = client_code
    #   Mailing_lists.client_teg.save()
    #   print(Mailing_lists.client_teg)
    #   return Mailing_lists.client_teg